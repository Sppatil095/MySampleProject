//
//  SearchTrainDetailsViewController.swift
//  MyStationFinder
//
//  Created by Sudha P on 10/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import UIKit

class SearchTrainDetailsViewController: UIViewController {
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var detailsTableView: UITableView!
    var xmlParser = XMLParser()
    var trainElements:[String:String] = [:]
    var trains = ""
    var trainCode = ""
    var trainDate = ""
    var locationCode = ""
    var locationFullName = ""
    var trainOrigin = ""
    var trainDestination = ""
    var arrival = ""
    var departure = ""
    var trainEnteredCode = ""
    var searchDate = ""
    var trainSearchDetailsModel: [TrainSearchDetailsModel] = []
    var selectedDate = ""
    
    @IBOutlet weak var datePicker: UIDatePicker!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.detailsTableView.isHidden = true
        self.datePicker.isHidden = true
        // Do any additional setup after loading the view.
        let nibNameForGenericCell = UINib(nibName: String(describing: SelectedTrainDetailsTableViewCell.self) , bundle: nil)
        detailsTableView.register(nibNameForGenericCell, forCellReuseIdentifier: String(describing: SelectedTrainDetailsTableViewCell.self))
        detailsTableView.dataSource = self
        detailsTableView.delegate = self
        
        detailsTableView.rowHeight = UITableViewAutomaticDimension
        detailsTableView.estimatedRowHeight = 183
        
        self.codeTextField.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func dateButtonAction(_ sender: Any) {
        self.datePicker.isHidden = false
        showDatePicker()
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        self.getTrainDetails()
    }
    
    func getTrainDetails(){
        self.trainSearchDetailsModel.removeAll()
        let plistPath: String? = Bundle.main.path(forResource:"AppConfig", ofType: "plist")
        let uriConfigCache: NSDictionary? = plistPath != nil ? NSDictionary(contentsOfFile: plistPath!) : nil
        var urlString = uriConfigCache?.value(forKey: "getTrainMovementsWithIDAndDate") as? String
        //http://api.irishrail.ie/realtime/realtime.asmx/getTrainMovementsXML?TrainId=e109&TrainDate=21 dec 2011
        if codeTextField.text != "" && dateButton.titleLabel?.text != "" {
            if let code = codeTextField.text, let date = dateButton.titleLabel?.text, let urlStr = urlString {
                urlString = urlStr + "TrainId=\(code)&TrainDate=\(date)"
                urlString = urlString?.replacingOccurrences(of: " ", with: "%20")
                if let stringURL = urlString {
                    xmlParser = XMLParser(contentsOf: URL(string: stringURL)!)!
                }
            }
        }else {
            let alert = UIAlertController(title: "Alert", message: "Enter Train Code and Date", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK",
                                          style: UIAlertActionStyle.destructive,
                                          handler: {(_: UIAlertAction!) in
                                            //Sign out action
            }))
            self.present(alert, animated: true, completion: nil)
        }
        xmlParser.delegate = self
        xmlParser.parse()
    }
    
    @IBAction func datePickerChanged(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MM yyyy"
        dateButton.titleLabel?.text = formatter.string(from: datePicker.date)
        self.datePicker.isHidden = true
        self.selectedDate = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }

    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SearchTrainDetailsViewController : XMLParserDelegate {
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        trains = elementName as String
        if (elementName as NSString).isEqual(to: "objTrainMovements") {
            trainElements = [:]
            trainCode = ""
            trainDate = ""
            locationCode = ""
            locationFullName = ""
            trainOrigin = ""
            trainDestination = ""
            arrival = ""
            departure = ""
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if trains.isEqual("TrainCode") {
            trainCode.append(string)
        } else if trains.isEqual("TrainDate") {
            trainDate.append(string)
        }else if trains.isEqual("LocationCode") {
            locationCode.append(string)
        }else if trains.isEqual("LocationFullName") {
            locationFullName.append(string)
        }else if trains.isEqual("TrainOrigin") {
            trainOrigin.append(string)
        }else if trains.isEqual("TrainDestination") {
            trainDestination.append(string)
        }else if trains.isEqual("Arrival") {
            arrival.append(string)
        }else if trains.isEqual("Departure") {
            departure.append(string)
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if (elementName as NSString).isEqual(to: "objTrainMovements") {
            let searchTrainModel = TrainSearchDetailsModel(trainCode:trainCode, trainDate: trainDate, locationCode: locationCode, locationFullName: locationFullName, trainOrigin: trainOrigin, trainDestination: trainDestination, arrival:arrival, departure:departure)
            
            trainSearchDetailsModel.append(searchTrainModel)
        }
        if trainSearchDetailsModel.count > 0 {
            self.detailsTableView.isHidden = false
            detailsTableView!.reloadData()
        }
    }
}

extension SearchTrainDetailsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trainSearchDetailsModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let genericCell = tableView.dequeueReusableCell(withIdentifier: String(describing: SelectedTrainDetailsTableViewCell.self)) as! SelectedTrainDetailsTableViewCell
        genericCell.trainCodeLabel?.text = "\(trainSearchDetailsModel[indexPath.row].trainCode)"
        genericCell.trainDateLabel?.text = "\(trainSearchDetailsModel[indexPath.row].trainDate)"
        genericCell.locationCodeLabel?.text = "Location Code : \(trainSearchDetailsModel[indexPath.row].locationCode)"
        genericCell.locationNameLabel?.text = "Location Name : \(trainSearchDetailsModel[indexPath.row].locationFullName)"
        genericCell.trainOriginLabel?.text = "Origin : \(trainSearchDetailsModel[indexPath.row].trainOrigin)"
        genericCell.trainDestiLabel?.text = "Destination : \(trainSearchDetailsModel[indexPath.row].trainDestination)"
        genericCell.trainArrivalLabel?.text = "Arrival : \(trainSearchDetailsModel[indexPath.row].arrival)"
        genericCell.trainDepartureLabel?.text = "Departure : \(trainSearchDetailsModel[indexPath.row].departure)"
        
        return genericCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 183
    }
}


extension SearchTrainDetailsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == codeTextField {
            if (codeTextField.text?.count)! > 3 {
                return false
            }
        }
        return true
    }
    
}
