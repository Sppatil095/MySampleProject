//
//  StationsListViewController.swift
//  MyStationFinder
//
//  Created by Sudha P on 09/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import UIKit

class StationsListViewController: UIViewController {

    var xmlParser = XMLParser()
    var stationElements:[String:String] = [:]
    var station = ""
    var stationDesc = ""
    var stationAlias = ""
    var stationLatitude = ""
    var stationLongitude = ""
    var stationCode = ""
    var stationId = ""
    var stationDetailsModel: [StationDetails] = []
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var stationTableView: UITableView!
    var selectedSegmentIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view, typically from a nib.
        let nibNameForGenericCell = UINib(nibName: String(describing: StationDetailsCell.self) , bundle: nil)
        stationTableView.register(nibNameForGenericCell, forCellReuseIdentifier: String(describing: StationDetailsCell.self))
        stationTableView.dataSource = self
        stationTableView.delegate = self
        
        stationTableView.rowHeight = UITableViewAutomaticDimension
        stationTableView.estimatedRowHeight = 88
        getParse()
    }
    
    func getParse(){
        self.stationDetailsModel.removeAll()
        let plistPath: String? = Bundle.main.path(forResource:"AppConfig", ofType: "plist")
        let uriConfigCache: NSDictionary? = plistPath != nil ? NSDictionary(contentsOfFile: plistPath!) : nil
        
        var urlString = selectedSegmentIndex == 0 ? uriConfigCache?.value(forKey: "getAllStations") as? String : uriConfigCache?.value(forKey: "getAllStationswithType") as? String
        if let urlStr = urlString {
            if selectedSegmentIndex == 1 {
                urlString = urlStr + "=M"
            }else if selectedSegmentIndex == 2{
                urlString = urlStr + "=S"
            }else if selectedSegmentIndex == 3 {
                urlString = urlStr + "=D"
            }
        }
        if let urlString = urlString {
            xmlParser = XMLParser(contentsOf: URL(string: urlString)!)!
        }
        xmlParser.delegate = self
        xmlParser.parse()
    }
    
    @IBAction func segmentControlAction(_ sender: UISegmentedControl) {
        self.selectedSegmentIndex = sender.selectedSegmentIndex
        self.getParse()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension StationsListViewController : XMLParserDelegate {
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        station = elementName as String
        if (elementName as NSString).isEqual(to: "objStation") {
            stationElements = [:]
            stationDesc = ""
            stationAlias = ""
        }
    }
    
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if station.isEqual("StationDesc") {
            stationDesc.append(string)
        } else if station.isEqual("StationAlias") {
            stationAlias.append(string)
        }else if station.isEqual("StationLatitude") {
            stationLatitude.append(string)
        }else if station.isEqual("StationLongitude") {
            stationLongitude.append(string)
        }else if station.isEqual("StationCode") {
            stationCode.append(string)
        }else if station.isEqual("StationId") {
            stationId.append(string)
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if (elementName as NSString).isEqual(to: "objStation") {
            let stationD = StationDetails(stationDesc:stationDesc, stationAlias: stationAlias, stationLatitude: stationLatitude, stationLongitude: stationLongitude, stationCode: stationCode, stationId: stationId)//StationDetails(stationDesc: stationDesc, stationAlias: stationAlias)
            stationDetailsModel.append(stationD)
        }
        stationTableView!.reloadData()
    }
}

extension StationsListViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stationDetailsModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let genericCell = tableView.dequeueReusableCell(withIdentifier: String(describing: StationDetailsCell.self)) as! StationDetailsCell
        genericCell.stationNameLabel?.text = stationDetailsModel[indexPath.row].stationDesc
        genericCell.stationCodeLabel?.text = "Code : \(stationDetailsModel[indexPath.row].stationCode)"
        genericCell.stationId?.text = "ID : \(stationDetailsModel[indexPath.row].stationId)"
        
        
        return genericCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
    
}
