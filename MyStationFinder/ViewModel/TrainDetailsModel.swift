//
//  TrainDetailsModel.swift
//  MyStationFinder
//
//  Created by Sudha P on 09/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import Foundation
struct TrainDetails {
    var trainStatus: String
    var trainLatitude: String
    var trainLongitude: String
    var trainCode: String
    var trainDate: String
    var publicMessage: String
    var direction: String
    
    init(trainStatus:String, trainLatitude: String, trainLongitude: String, trainCode: String, trainDate: String, publicMessage: String, direction:String) {
        self.trainStatus = trainStatus
        self.trainLatitude = trainLatitude
        self.trainLongitude = trainLongitude
        self.trainCode = trainCode
        self.trainDate = trainDate
        self.publicMessage = publicMessage
        self.direction = direction
    }
}
