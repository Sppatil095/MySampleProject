//
//  TrainSearchDetailsModel.swift
//  MyStationFinder
//
//  Created by Sudha P on 10/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import Foundation

struct TrainSearchDetailsModel {
    var trainCode: String
    var trainDate: String
    var locationCode: String
    var locationFullName: String
    var trainOrigin: String
    var trainDestination: String
    var arrival: String
    var departure: String
    
    init(trainCode:String, trainDate: String, locationCode: String, locationFullName: String, trainOrigin: String, trainDestination: String, arrival:String, departure:String) {
        self.trainCode = trainCode
        self.trainDate = trainDate
        self.locationCode = locationCode
        self.locationFullName = locationFullName
        self.trainOrigin = trainOrigin
        self.trainDestination = trainDestination
        self.arrival = arrival
        self.departure = departure
    }
}
