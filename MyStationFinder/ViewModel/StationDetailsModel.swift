//
//  StationDetailsModel.swift
//  MyStationFinder
//
//  Created by Sudha P on 09/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import Foundation

struct StationDetails {
    var stationDesc: String
    var stationAlias: String
    var stationLatitude: String
    var stationLongitude: String
    var stationCode: String
    var stationId: String
    
    init(stationDesc:String, stationAlias: String, stationLatitude: String, stationLongitude: String, stationCode: String, stationId: String) {
        self.stationDesc = stationDesc
        self.stationAlias = stationAlias
        self.stationLatitude = stationLatitude
        self.stationLongitude = stationLongitude
        self.stationCode = stationCode
        self.stationId = stationId
    }
}
