//
//  TrainDetailsTableViewCell.swift
//  MyStationFinder
//
//  Created by Sudha P on 09/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import UIKit

class TrainDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var trainCodeLabel: UILabel!
    @IBOutlet weak var trainDateLabel: UILabel!
    @IBOutlet weak var trainDetailLabel: UILabel!
    @IBOutlet weak var TrainDirectionLabel: UILabel!
    @IBOutlet weak var trainRunningStatusLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
