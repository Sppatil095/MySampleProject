//
//  SelectedTrainDetailsTableViewCell.swift
//  MyStationFinder
//
//  Created by Sudha P on 10/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import UIKit

class SelectedTrainDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var trainCodeLabel: UILabel!
    @IBOutlet weak var trainDateLabel: UILabel!
    @IBOutlet weak var locationCodeLabel: UILabel!
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var trainOriginLabel: UILabel!
    @IBOutlet weak var trainDestiLabel: UILabel!
    @IBOutlet weak var trainArrivalLabel: UILabel!
    @IBOutlet weak var trainDepartureLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
