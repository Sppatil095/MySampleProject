//
//  StationDetailsCell.swift
//  MyStationFinder
//
//  Created by Sudha P on 09/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import UIKit

class StationDetailsCell: UITableViewCell {
    @IBOutlet weak var stationNameLabel: UILabel!
    
    @IBOutlet weak var stationCodeLabel: UILabel!
    @IBOutlet weak var stationId: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
