//
//  CurrentTrainListViewController.swift
//  MyStationFinder
//
//  Created by Sudha P on 09/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import UIKit

class CurrentTrainListViewController: UIViewController {
    var xmlParser = XMLParser()
    var trainElements:[String:String] = [:]
    var trains = ""
    var trainStatus = ""
    var trainLatitude = ""
    var trainLongitude = ""
    var trainCode = ""
    var trainDate = ""
    var publicMessage = ""
    var direction = ""
    var trainDetailsModel: [TrainDetails] = []
    var selectedSegmentIndex = 0
    @IBOutlet weak var listTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let nibNameForGenericCell = UINib(nibName: String(describing: TrainDetailsTableViewCell.self) , bundle: nil)
        listTableView.register(nibNameForGenericCell, forCellReuseIdentifier: String(describing: TrainDetailsTableViewCell.self))
        listTableView.dataSource = self
        listTableView.delegate = self
        
        listTableView.rowHeight = UITableViewAutomaticDimension
        listTableView.estimatedRowHeight = 179
        getTrainDetails()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func getTrainDetails(){
        self.trainDetailsModel.removeAll()
        let plistPath: String? = Bundle.main.path(forResource:"AppConfig", ofType: "plist")
        let uriConfigCache: NSDictionary? = plistPath != nil ? NSDictionary(contentsOfFile: plistPath!) : nil
        var urlString = selectedSegmentIndex == 0 ? uriConfigCache?.value(forKey: "getCurrentTrains") as? String : uriConfigCache?.value(forKey: "getCurrentTrainswithType") as? String
        if let urlStr = urlString {
            if selectedSegmentIndex == 1 {
                urlString = urlStr + "=M"
            }else if selectedSegmentIndex == 2{
                urlString = urlStr + "=S"
            }else if selectedSegmentIndex == 3 {
                urlString = urlStr + "=D"
            }
        }
        if let urlString = urlString {
            xmlParser = XMLParser(contentsOf: URL(string: urlString)!)!
        }
        xmlParser.delegate = self
        xmlParser.parse()
    }
    @IBAction func segmentControlAction(_ sender: UISegmentedControl) {
        self.selectedSegmentIndex = sender.selectedSegmentIndex
        self.getTrainDetails()
    }
}


extension CurrentTrainListViewController : XMLParserDelegate {
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        trains = elementName as String
        if (elementName as NSString).isEqual(to: "objTrainPositions") {
            trainElements = [:]
            trainStatus = ""
            trainLatitude = ""
            trainLongitude = ""
            trainCode = ""
            trainDate = ""
            publicMessage = ""
            direction = ""
        }
    }
    
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if trains.isEqual("TrainStatus") {
            trainStatus.append(string)
        } else if trains.isEqual("TrainLatitude") {
            trainLatitude.append(string)
        }else if trains.isEqual("TrainLongitude") {
            trainLongitude.append(string)
        }else if trains.isEqual("TrainCode") {
            trainCode.append(string)
        }else if trains.isEqual("TrainDate") {
            trainDate.append(string)
        }else if trains.isEqual("PublicMessage") {
            publicMessage.append(string)
        }else if trains.isEqual("Direction") {
            direction.append(string)
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if (elementName as NSString).isEqual(to: "objTrainPositions") {
            let trainD = TrainDetails(trainStatus:trainStatus, trainLatitude: trainLatitude, trainLongitude: trainLongitude, trainCode: trainCode, trainDate: trainDate, publicMessage: publicMessage, direction:direction)
            trainDetailsModel.append(trainD)
        }
        listTableView!.reloadData()
    }
}

extension CurrentTrainListViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trainDetailsModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let genericCell = tableView.dequeueReusableCell(withIdentifier: String(describing: TrainDetailsTableViewCell.self)) as! TrainDetailsTableViewCell
        genericCell.trainCodeLabel?.text = "Train Code : \(trainDetailsModel[indexPath.row].trainCode)"
        genericCell.trainDateLabel?.text = "Train Date : \(trainDetailsModel[indexPath.row].trainDate)"
        genericCell.trainDetailLabel?.text = "Details : \(trainDetailsModel[indexPath.row].publicMessage)"
        genericCell.TrainDirectionLabel?.text = "Direction : \(trainDetailsModel[indexPath.row].direction)"
        if trainDetailsModel[indexPath.row].trainStatus == "N\n    " {
            genericCell.trainRunningStatusLabel?.text = "Not Running"
            genericCell.trainRunningStatusLabel?.textColor = UIColor.red
        }else {
            genericCell.trainRunningStatusLabel?.text = "Running"
            genericCell.trainRunningStatusLabel?.textColor = UIColor.green
        }
        
        return genericCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 179
    }
    
}
